
(async () => {
    const lSequelize = require('./connect/sequelize')
    const lClientSchema = require('./bd/cliente')
 
    try {
       lClientSchema(lSequelize)

       lSequelize.sync({force: true})
        
  

    } catch (error) {
        console.log(error);
    }
})();