const restify = require('restify')
const config = require('./configs/config')
const  servidor = restify.createServer()
const cors = require('./cors') 
//const bodyParser = require('body-parser')
//const urlEncodedParser = bodyParser.urlencoded({extended: false})



const {insertIntoClient, findClients} = require('./model/cliente.model')

//const pool  = require('./connect/pool')

//const table = require('./bd/criar_tabelas')


servidor.pre(cors.preflight)
servidor.use(cors.actual)
servidor.use(restify.plugins.bodyParser({
    maxBodySize:0,
    mapParams:true,
    mapFile:false
}))

servidor.get('/client', async  (req,res)=>{
   const listClients = await findClients() 
    res.json(listClients)
})

servidor.get('/client/:id', async  (req,res)=>{
    const listClients = await findClients(req.params.id, null) 
     res.json(listClients)
 })

 servidor.get('/client/where/:field/:value', async  (req,res)=>{
    let objWhere = {where:{}}
    objWhere.where[req.params.field] = req.params.value

    console.log(objWhere)
    const listClients = await findClients(null, objWhere) 
     res.json(listClients)
 })


servidor.post('/client', async (req,res)=>{
const lInsert = await insertIntoClient(
    req.params.name,
    req.params.publicPlace,
    req.params.number,
    req.params.complement,
    req.params.district,
    req.params.state,
    req.params.city,
    req.params.country,
    req.params.zipCode,
    req.params.registerNumber,
    req.params.email,
    req.params.phoneNumber
)
    
    res.json(lInsert)


})

servidor.listen(config.port,()=>{
    console.log('servidor rodando porta '+config.port)
})
