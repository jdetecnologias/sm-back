async function handleMsgError(ACallBack){
    let lObjResturn = {}
    try {

        await ACallBack()

     }catch(e){
 
         lObjResturn = {status: false, msn:e.errors[0].message}
 
     }finally{
 
         if (lObjResturn.status === undefined){
             lObjResturn = {status: true, msn:'Created successfuly!'}
         }
         return  lObjResturn
     }
}

module.exports = {handleMsgError}