const {handleMsgError} = require('../utils/functions')

async function findClients(Apk = null, AWhere = null){ // Apk id number, AWhere object field:value
    const lSequelize = require('../connect/sequelize')
    const lClientSchema = require('../bd/client')    
    lClient = await lClientSchema(lSequelize)
    let lRows = []

    async function getListClients(){

        if (Apk){
            lRows = await lClient.findByPk(Apk)    
        }else if(AWhere){
            lRows = await lClient.findAll(AWhere) 
        }else{
            lRows = await lClient.findAll()
        }
        
    }

    const lMsn =  await handleMsgError(getListClients) 

    return !lMsn.status ? lMsn : lRows
}

async function insertIntoClient(AName, APublicPlace, ANumber, AComplement, ADistrict, AState, ACity, ACrountry, AZipCode, ARegisterNumber, AEmail, APhoneNumber){
    const lSequelize = require('../connect/sequelize')
    const lClientSchema = require('../bd/client')

    lClient = await lClientSchema(lSequelize)

    const lObjectInsert = {
        name:AName, 
        publicPlace:APublicPlace, 
        number: ANumber, 
        complement: AComplement, 
        district: ADistrict, 
        city: ACity, 
        state: AState,
        country: ACrountry, 
        zipCode: AZipCode, 
        registerNumber: ARegisterNumber, 
        email: AEmail, 
        phoneNumber: APhoneNumber       
    }
    
    return handleMsgError(()=>lClient.create(lObjectInsert))

}

module.exports = {findClients, insertIntoClient}