async function clientSchema(ASequelize){ 
    const {DataTypes} =  require('sequelize')
    
    const lClient = await ASequelize.define('client',{
        id:{
            type: DataTypes.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        name:{type:DataTypes.STRING, allowNull: false},
        publicPlace:{type:DataTypes.STRING, allowNull: false},
        number:{type:DataTypes.STRING, allowNull: true},
        complement:{type:DataTypes.STRING, allowNull: true},
        district:{type:DataTypes.STRING, allowNull: true},
        city:{type:DataTypes.STRING, allowNull: true},
        state:{type:DataTypes.STRING, allowNull: true},
        country:{type:DataTypes.STRING, allowNull: true},
        zipCode:{type:DataTypes.STRING, allowNull: true},
        registerNumber:{type:DataTypes.STRING, allowNull: false,  unique: true},
        email:{type:DataTypes.STRING, allowNull: false, unique: true},
        phoneNumber:{type:DataTypes.STRING, allowNull: false,  unique: true}
    })
    
 return lClient 

}

module.exports = clientSchema
