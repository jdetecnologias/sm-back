
(async () => {
    const lSequelize = require('./connect/sequelize')
    const lClientSchema = require('./bd/client')
    const lProductSchema = require('./bd/product')
    const lOrderSchema = require('./bd/order')
    const lStockSchema = require('./bd/stock')

    const CreateTriggersDB = require('./bd/triggers/triggers.DB')
    const CreateFunctionsDB = require('./bd/functions/functions.DB')

 
    try {
       await lClientSchema(lSequelize)
       const  lProduct = await lProductSchema(lSequelize)
       const lOrder = await lOrderSchema(lSequelize,(AIOrdem)=>{
        AIOrdem.belongsTo(lProduct)

        lProduct.hasMany(AIOrdem)
       })

       const stock = await lStockSchema(lSequelize,(AStock,AMovimentedStock)=>{
            AStock.belongsTo(lProduct)
            AMovimentedStock.belongsTo(lProduct)
            lProduct.hasOne(AStock)
            lProduct.hasMany(AMovimentedStock)
       })
       await  lSequelize.sync({force:true})
       await CreateFunctionsDB(lSequelize)
       await CreateTriggersDB(lSequelize)
       await  lSequelize.sync({force:true})
         
      


  

    } catch (error) {
        console.log(error);
    }
})();
