module.exports = (DataTypes)=>({
    CLIENT_SCHEMA:{
        id:{
            type: DataTypes.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        name:{type:DataTypes.STRING, allowNull: false},
        publicPlace:{type:DataTypes.STRING, allowNull: false},
        number:{type:DataTypes.STRING, allowNull: true},
        complement:{type:DataTypes.STRING, allowNull: true},
        district:{type:DataTypes.STRING, allowNull: true},
        city:{type:DataTypes.STRING, allowNull: true},
        state:{type:DataTypes.STRING, allowNull: true},
        country:{type:DataTypes.STRING, allowNull: true},
        zipCode:{type:DataTypes.STRING, allowNull: true},
        registerNumber:{type:DataTypes.STRING, allowNull: false,  unique: true},
        email:{type:DataTypes.STRING, allowNull: false, unique: true},
        phoneNumber:{type:DataTypes.STRING, allowNull: false,  unique: true}
    },
    PRODUCT_SCHEMA:{
        id:{
            type: DataTypes.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        idParent:{
            type: DataTypes.INTEGER,
            allowNull: true,        
        },
        sku:{type:DataTypes.STRING, allowNull: false},        
        name:{type:DataTypes.STRING, allowNull: false},
        price:{type:DataTypes.FLOAT,allowNull:true},
        weight:{type:DataTypes.FLOAT, allowNull: true},
    }, 
    ORDER_SCHEMA:{
        id:{
            type: DataTypes.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        idClient:{
            type: DataTypes.INTEGER,
            allowNull: false
        },     
        status:{
            type: DataTypes.ENUM('Pendent', 'Done'),
            allowNull: false
        }              
    },
    ITEM_ORDER_SCHEMA:{
        id:{
            type: DataTypes.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        qty:{type:DataTypes.INTEGER, allowNull: false},        
        price:{type:DataTypes.FLOAT,allowNull:true},
    },
    STOCK_SCHEMA:{
        id:{
            type: DataTypes.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },     
        availQty:{
            type: DataTypes.FLOAT,
            allowNull: false
        },
        AlocQty:{
            type: DataTypes.FLOAT,
            allowNull: false
        }                  
    },
    MOVIMENTED_STOCK_SCHEMA:{
        id:{
            type: DataTypes.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },     
        qty:{
            type: DataTypes.FLOAT,
            allowNull: false
        },
        kind:{
            type: DataTypes.ENUM('Debit', 'Credit'),
            allowNull: false
        }                  
    }    
})