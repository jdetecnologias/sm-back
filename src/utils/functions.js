async function handleMsgError(ACallBack){
    let lObjResturn = {}
    try {

        await ACallBack()

     }catch(e){
        
         lObjResturn = {status: false, msn:getMsgError(e.errors)}
 
     }finally{
 
         if (lObjResturn.status === undefined){
             lObjResturn = {status: true, msn:'Created successfuly!'}
         }

         console.log(lObjResturn)
         return  lObjResturn
     }
}


function getMsgError(AErrorMsgs){
    let lmsg = ""

    AErrorMsgs.forEach(function(error){
    
        lmsg += error.message + ', '
    })

    return lmsg
}

module.exports = {handleMsgError}