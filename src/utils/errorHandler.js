async function handleMsgError(ACallBack){
    let lObjResturn = {}
    let lError = false
    try {

        await ACallBack()

     }catch(e){
        lError = true
         lObjResturn = {status: false, msn:e.message}
         
     }finally{
 
         if (!lError){
             lObjResturn = {status: true, msn:'Created successfuly!'}
         }
         
         return  lObjResturn
     }
}


module.exports = {handleMsgError}