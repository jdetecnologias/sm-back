const movimentedstocksTrigger =
'CREATE TRIGGER updates_stocks '+
'AFTER INSERT '+
'ON public.movimentedstocks '+
'FOR EACH ROW '+
'EXECUTE PROCEDURE public.updating_stock()';


module.exports = movimentedstocksTrigger