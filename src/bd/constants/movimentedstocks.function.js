const movimentedstockFunction = `
CREATE OR REPLACE FUNCTION public.updating_stock()
   RETURNS trigger
     LANGUAGE "plpgsql"
 AS $BODY$
 declare
     r_stocks record;
    c_stocks cursor for select "availQty", "AlocQty" from stocks where "productId" = new."productId";
    l_avail_qty float;
    l_aloc_qty float;
 begin
 open c_stocks;
 fetch c_stocks into r_stocks;
 if new.kind = "Debit" then
     l_avail_qty := r_stocks."availQty" + new.qty;
    l_aloc_qty := r_stocks."AlocQty";
 end if; 
 if new.kind= "Credit" then
    l_avail_qty := r_stocks."availQty" - new.qty;
    l_aloc_qty := r_stocks."AlocQty";	
 end if;
 update stocks set "availQty" = l_avail_qty, "AlocQty" = l_aloc_qty, "updatedAt" = current_timestamp where "productId" = new."productId";
 return new;
 end;
 $BODY$; `



module.exports = movimentedstockFunction

