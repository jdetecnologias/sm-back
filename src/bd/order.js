async function OrderSchema(ASequelize, ACallback = null){ 
    const {DataTypes} =  require('sequelize')
    const SCHEMAS = require('../constants/constants')
    
    const lOrder = await ASequelize.define('order',SCHEMAS(DataTypes).ORDER_SCHEMA)

    const lItemOrder = await ASequelize.define('iorder',SCHEMAS(DataTypes).ITEM_ORDER_SCHEMA)

    lOrder.hasMany(lItemOrder)

    lItemOrder.belongsTo(lOrder)
    
    if (ACallback) ACallback(lItemOrder)
    
    return lOrder

}

module.exports = OrderSchema
