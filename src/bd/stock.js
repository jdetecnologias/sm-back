async function AStockSchema(ASequelize, ACallback = null){ 
    const {DataTypes} =  require('sequelize')
    const SCHEMAS = require('../constants/constants')
    
    const lStock = await ASequelize.define('stock',SCHEMAS(DataTypes).STOCK_SCHEMA)
    const lMovimentStock = await ASequelize.define('movimentedstock',SCHEMAS(DataTypes).MOVIMENTED_STOCK_SCHEMA)


    if (ACallback)ACallback(lStock,lMovimentStock)
    
    return lStock

}

module.exports = AStockSchema
