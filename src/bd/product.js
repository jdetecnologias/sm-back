async function ProductSchema(ASequelize){ 
    const {DataTypes} =  require('sequelize')
    const SCHEMAS = require('../constants/constants')    
    
    const lProduct = await ASequelize.define('product',SCHEMAS(DataTypes).PRODUCT_SCHEMA)
    
 return lProduct 

}

module.exports = ProductSchema
