async function CreateFunctionsDB(){
    const lSequelize = require('sequelize')
    const movimentedstocksFunction = require('./constants/movimentedstocks.function')
    const movimentedstocksTrigger = require('./constants/movimentedstocks.trigger')
    await lSequelize.query(movimentedstocksFunction)
    await lSequelize.query(movimentedstocksTrigger) 

    lSequelize.sync({force: true})
}

module.exports = CreateFunctionsDB