async function clientSchema(ASequelize){ 
    const {DataTypes} =  require('sequelize')
    const SCHEMAS = require('../constants/constants')
    
    const lClient = await ASequelize.define('client',SCHEMAS(DataTypes).CLIENT_SCHEMA)
    
 return lClient 

}

module.exports = clientSchema
