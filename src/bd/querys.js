const client = {create: `create table if not exists CLIENTS(
    id serial PRIMARY KEY,
     name varchar not null, 
     public_area varchar, 
     numberStreet varchar, 
     complement varchar,
     district varchar,
     city varchar,
     state varchar,
     country varchar,
     zip_code varchar,
     entity varchar not null, 
     create_at timestamp,
     update_at timestamp)
    `,
    phone_number:
    ` CREATE TABLE IF NOT EXISTS
      PHONE_NUMBER(ID SERIAL PRIMARY KEY,ID_CLIENT INT, CONSTRAINT fk_client_phone_number FOREIGN KEY(ID_CLIENT) REFERENCES CLIENTS(ID), NUMBER INTEGER NOT NULL)`
    }

    module.exports = {client}
