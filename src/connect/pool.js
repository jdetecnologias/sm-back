async function connect(){

    const lconnectionString = 'postgres://postgres:masterkey@localhost:5432/sales'

    if(global.connection){
        return global.connection.connect()
    }

    const {Pool} = require('pg')


    const lPool = new Pool({
        connectionString:lconnectionString
    })

    const lClient = await lPool.connect()

    const lRes = await lClient.query('SELECT NOW()')

    lClient.release()

    global.connection = lPool

    return  lPool.connect()


}

module.exports = connect


