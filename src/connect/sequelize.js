const {Sequelize, Model, Datatypes} = require('sequelize')
const config = require('../configs/config')

const sequelize = new Sequelize(config.stringConnectionPostGres,{dialect: 'postgres'})

module.exports =  sequelize

