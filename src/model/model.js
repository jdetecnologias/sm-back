class Model{

  constructor(ATable){
    this.GSequelize = require('../connect/sequelize')
    const {handleMsgError} = require('../utils/errorHandler')
    this.handleMsgError = handleMsgError
    this.FRequirePath = '../bd/'+ATable
    this.TTableSchema =  require(this.FRequirePath)
  } 

  async FTable(){

    return await this.TTableSchema(this.GSequelize)

  }

  async findInTable(Apk = null, AWhere = null){ // Apk id number, AWhere object field:value
      const lTable = await this.FTable()
      let lRows = []
      
      async function getListTable(){
          if (Apk){
              lRows = await lTable.findByPk(Apk)    
          }else if(AWhere){
              lRows = await lTable.findAll(AWhere) 
          }else{
              lRows = await lTable.findAll()
              
          }
          
      }

      const lMsn =  await this.handleMsgError(getListTable) 

      return !lMsn.status ? lMsn : lRows
  }

  async insertIntoTable(AObjectInsert){
    const lTable = await this.FTable()
  
      return this.handleMsgError(()=> lTable.create(AObjectInsert))

  }

}

module.exports = {Model}