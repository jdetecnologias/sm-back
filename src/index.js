const restify = require('restify')
const config = require('./configs/config')
const  servidor = restify.createServer()
const cors = require('./cors') 
//const bodyParser = require('body-parser')
//const urlEncodedParser = bodyParser.urlencoded({extended: false})



const {insertIntoClient, findClients} = require('./model/cliente.model')

const {Model} = require('./model/model')

servidor.pre(cors.preflight)
servidor.use(cors.actual)
servidor.use(restify.plugins.bodyParser({
    maxBodySize:0,
    mapParams:true,
    mapFile:false
}))

servidor.get('/:table', async  (req,res)=>{


 
    const model = new Model(req.params.table)
    const listClients = await model.findInTable()
     res.json(listClients)
   
 })

servidor.post('/:table', async (req,res)=>{
    console.log(req.params) 
    const model =  new Model(req.params.table)

    const result = await model.insertIntoTable(req.params)

    res.json(result)
    
    
    })
servidor.get('/:table/:id', async  (req,res)=>{
    const model = new Model(req.params.table)

    const listClients = await model.findInTable(req.params.id, null) 

     res.json(listClients)
 })



 servidor.get('/:table/where/:field/:value', async  (req,res)=>{
     const model = new Model(req.params.table)
    let objWhere = {where:{}}
    objWhere.where[req.params.field] = req.params.value

    const listClients = await model.findInTable(null, objWhere) 
    
     res.json(listClients)
 })


/*servidor.post('/client', async (req,res)=>{
const lInsert = await insertIntoClient(
    req.params.name,
    req.params.publicPlace,
    req.params.number,
    req.params.complement,
    req.params.district,
    req.params.state,
    req.params.city,
    req.params.country,
    req.params.zipCode,
    req.params.registerNumber,
    req.params.email,
    req.params.phoneNumber
)
    
    res.json(lInsert)


})*/

servidor.listen(config.port,()=>{
    console.log('servidor rodando porta '+config.port)
})
